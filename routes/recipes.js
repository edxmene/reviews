var recipes = require("../recipes.json");
var router = require("express").Router();

router.get("/shopping-list", function (req, res) {
  let ingredients = [];
  if (!req.query.ids) {
    res.status(400).send();
    return;
  }
  const ids = req.query.ids.split(",");
  for (let id of ids) {
    const recipe = recipes.find((recipe_) => recipe_.id === parseInt(id));
    if (recipe) {
      ingredients.push(...recipe.ingredients);
    }
  }
  if (ingredients.length == 0) {
    res.status(404).send("NOT_FOUND");
    return;
  }
  res.status(200).send(ingredients);
});

module.exports = router;
